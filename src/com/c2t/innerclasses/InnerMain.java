package com.c2t.innerclasses;

class MyOuterClass{
	
	class InnerClass{
		public void methodInner(){
			System.out.println("Inner class Method....");
		}
	}
	
	public void methodOuter(){
		System.out.println("Outer class....");
		
		InnerClass in = new InnerClass();
		in.methodInner();
	}
	
}


public class InnerMain {
	
	public static void main(String[] args) {
		/*Outer out = new Outer();
		Outer.Inner in = out.new Inner();
		in.method();*/
		
		/*MyOuterClass out = new MyOuterClass();
		MyOuterClass.MyInnerClass in = out.new MyInnerClass();
		in.display();*/
		
		MyOuterClass outer = new MyOuterClass();
		outer.methodOuter();
		
		//InnerClass i = new InnerClass();
		
		MyOuterClass.InnerClass in = outer.new InnerClass();
		in.methodInner();
		
	}
}


