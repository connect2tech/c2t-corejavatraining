package com.c2t.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MyFirstArrayList {
	public static void main(String[] args) {

		// add(E e)
		// clear()
		// contains(Object o)
		// get(int index)
		// indexOf(Object o)
		
		List <String> l = new ArrayList<String>();
		l.add("C");
		l.add("A");
		l.add("A");
		System.out.println(l);
		
		/*for(int i=0;i<l.size();i++){
			String s = l.get(i);
			System.out.println();
			if(s.equals("shiva")){
				l.remove("shiva");
			}
		}
		
		System.out.println("--Iterator--");
		Iterator <String> iter = l.iterator();
		while(iter.hasNext()){
			String s = iter.next();
			
			if(s.equals("shiva")){
				iter.remove();
			}
			
			System.out.println(s);
		}
		
		System.out.println("--after removal---");
		System.out.println(l);*/
		
		
		//l.clear();
		//System.out.println(l);
		
		/*boolean b = l.contains("B");
		if(b){
			System.out.println("Element is present!!!");
		}else{
			System.out.println("Not Present");
		}*/
		
		
		// Generics
		/*
		 * ArrayList <String> al = new <String> ArrayList(); al.add("Jyoti");
		 * al.add("Milesh"); al.add("Soujanya"); al.add("Suchismita");
		 * al.add("Amogh"); al.add("Apeksha"); al.add("Apeksha");
		 * System.out.println(al);
		 */

		// 1st
		/*
		 * for(int i=0;i<al.size();i++){ System.out.println(al.get(i)); }
		 */

		// iterator
		// System.out.println("Iterator");
		/*
		 * Iterator <String> iter = al.iterator(); while(iter.hasNext()){ String
		 * value = iter.next(); System.out.println(value); }
		 */

		// for loop
		/*
		 * for(String s:al){ System.out.println("value of s is::"+s); }
		 */

		/*
		 * Enumeration <String> e = Collections.enumeration(al);
		 * 
		 * while(e.hasMoreElements()){ String s = e.nextElement();
		 * System.out.println("s="+s); }
		 */

	}
}
