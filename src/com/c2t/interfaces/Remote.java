package com.c2t.interfaces;

public interface Remote {
	abstract void power();
	void increaseVolume();
}

interface I extends Remote{
	void i();
}

class TVRemote implements Remote {
	public void power() {
		//logic1
	}

	public void increaseVolume() {
		// TODO Auto-generated method stub
		
	}
}

class MusicSystem implements Remote {
	public void power() {
		//logic2
	}

	public void increaseVolume() {
		// TODO Auto-generated method stub
		
	}
}


class Multi{
	
}


