package com.c2t.oops;

public class MainStudent2 {
	public static void main(String[] args) {
		Student2 s1 = new Student2();
		s1.name="AAA";
		
		Student2 s2 = new Student2();
		s2.name="BBB";
		
		s1.instructor = "Naresh";
		
		/*//s1.displayName();
		s2.displayName();
		
		
		s2.instructor = "new instruction";
		
		s1.displayName();
		s2.displayName();
		
		
		Student2.instructor = "class variable";*/
		
		s1.teach();
		s2.teach();
		
		Student2.teach();
		
	}
}
