class Building {

	String name;
	int age;

	void display() {
		System.out.println("Name of the build is ::" + name);
	}
	
	Building(){
		name = "Mont Vert";
	}
	
	Building(String s){
		name = s;
	}
	
	Building(int i){
		age = i;
	}
}

public class Constructors {
	public static void main(String[] args) {
		System.out.println("Hello...");
		
		Building b1 = new Building();
		//b1.name = "Mont Vert";
		b1.display();
		
		Building b2 = new Building();
		//b2.name = "AAA";
		b2.display();
		
		Building b3 = new Building();
		//b3.name = "BBB";
		b3.display();
		
		
		Building b11 = new Building("Builder1");
		Building b22 = new Building("Builder2");
		Building b33 = new Building("Builder3");
		
		b11.display();
		b22.display();
		b33.display();
		
		Building b111 = new Building(5);
		
	}
}
