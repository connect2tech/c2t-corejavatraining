package com.c2t.loops;

public class ForLoopsNested {
	public static void main(String[] args) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.println("value of i =" + i + " , value of j = " + j);
			}
		}
	}
}
