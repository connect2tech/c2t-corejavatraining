package com.c2t.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class SortingArrayList {
	
	
	public static void main(String[] args) {
		List students = new ArrayList();
		
		students.add("Sush");
		students.add("Ishan");
		students.add("Naresh");
		
		System.out.println(students);
		
		Collections.sort(students);
		
		System.out.println(students);
		
	}

}
