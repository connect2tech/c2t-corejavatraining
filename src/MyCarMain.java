
class MyCar {
	
	String color;
	
	void startEngine() {
		System.out.println("I am staring engine...");
	}
	
	void display(){
		System.out.println("color="+color);
	}
	
	MyCar(String s){
		color = s;
		System.out.println("I am constructor...");
	}

	
	//Default constuctor
	MyCar(){
		color = "Red";
		System.out.println("I am constructor...");
	}
}

class MyMaruti extends MyCar{
	MyMaruti(){
		System.out.println("I am MyMaruti constructor...");
	}
}

public class MyCarMain {
	public static void main(String[] args) {
		MyCar c1 = new MyCar();
		c1.display();
		
		MyCar c2 = new MyCar();
		c2.display();
		
		MyCar c3 = new MyCar("blue");
		
		//MyCar mcar = new MyCar();
		
		//MyMaruti maruti = new MyMaruti();
		
		/*mcar.startEngine();
		MyMaruti mm = new MyMaruti();
		mm.startEngine();
		MyHyundai mh = new MyHyundai();
		mh.startEngine();*/
	}
}
