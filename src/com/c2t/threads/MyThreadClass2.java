package com.c2t.threads;

class AnotherClass {

}

class ThreadRunnable extends AnotherClass implements Runnable {

	public void run() {
		System.out.println("I am Thread !!!");
		System.out.println("My Name in run is ::" + Thread.currentThread().getName());
	}
}

public class MyThreadClass2 {
	public static void main(String[] args) {

		System.out.println("My Name in main is ::" + Thread.currentThread().getName());
		ThreadRunnable run = new ThreadRunnable();
		Thread t = new Thread(run,"My Thread");
		t.start();

	}
}
