package com.c2t.primitives;

public class UpCastingDownCasting {
	public static void main(String[] args) {
		
		int a = 10;
		long b;
		b = a;
		
		System.out.println("b="+b);
		
		
		long l = 10;
		int a1;
		
		a1 = (int)l;
		System.out.println("a1="+a1);
		
		
		double d = 10.5;
		
		int a2;
		a2 = (int)d;
		System.out.println("a2="+a2);
		
		
	}
}
