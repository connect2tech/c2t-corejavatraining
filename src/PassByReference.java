
public class PassByReference {

	int a = 100;

	public static void main(String[] args) {

		PassByReference ref = new PassByReference();
		System.out.println("Before---" + ref.a);
		changeValueUsingRef(ref);
		System.out.println("After---" + ref.a);
	}

	static void changeValueUsingRef(PassByReference temp) {
		temp.a = 200;
	}
}
