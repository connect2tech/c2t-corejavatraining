package com.c2t.strings;

public class Sbuffer {
	public static void main(String[] args) {
		
		System.out.println("----------sb-------------");
		StringBuffer sb = new StringBuffer();
		System.out.println(sb.capacity());
		System.out.println(sb.length());
		
		
		System.out.println("-----------sb2------------");
		StringBuffer sb2 = new StringBuffer("abcd");
		System.out.println(sb2.capacity());
		System.out.println(sb2.length());
		
		
		System.out.println("----------sb3-------------");
		
		StringBuffer sb3 = new StringBuffer();
		sb3.append("abcde");
		System.out.println(sb3.capacity());
		System.out.println(sb3.length());
		
		System.out.println("------------sb4-----------");
		
		StringBuffer sb4 = new StringBuffer();
		sb4.append("012345678912345678");
		System.out.println(sb4.capacity());
		System.out.println(sb4.length());
		

	}
}
