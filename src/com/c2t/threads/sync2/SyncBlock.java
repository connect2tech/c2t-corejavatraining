package com.c2t.threads.sync2;

class PrintTables {
	public void printTables(int number) {
		
		//statement1
		//statement2
		
		synchronized (this) {
			// System.out.println("number=="+number);
			for (int i = 1; i <= 10; i++) {
				System.out.println((number + " * " + i + " = ") + (number * i));
				// System.out.println("Thread.currentThread().getName()==" +
				// Thread.currentThread().getName());

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	
		//statement3
		//statement4
	
	}
}

class Thread1 extends Thread {

	PrintTables pt1;

	Thread1(PrintTables p) {
		pt1 = p;
	}

	public void run() {
		pt1.printTables(5);
	}
}

class Thread2 extends Thread {

	PrintTables pt1;

	Thread2(PrintTables p) {
		pt1 = p;
	}

	public void run() {
		pt1.printTables(10);
	}
}

public class SyncBlock {

	public static void main(String[] args) {
		PrintTables printTable = new PrintTables();
		Thread1 t1 = new Thread1(printTable);
		Thread2 t2 = new Thread2(printTable);

		t1.start();
		t2.start();
	}

}
