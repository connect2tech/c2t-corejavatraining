package com.c2t.oops;

interface inter1{
	void m1();
}

interface inter2{
	void m2();
}

interface inter3 extends inter1 , inter2{
	void m3();
}


public interface Remote {// Specifications or contract
	
	public static final String BUTTON = "buttonOn";
	
	abstract void powerOn();

	void increaseSound(); //implicitly method is abstract
	//public abstract void increaseSound();
}

//The type TVRemote must implement the inherited abstract method Remote.increaseSound()
abstract class TVRemote implements Remote{
	public void powerOn() {
		
		System.out.println();
		//System.out.println("TV Remote");
		//algorithm1
		
	}

	
	
}

class GamingRemote implements Remote {
	public void powerOn() {
		//algorithm2
	}

	public void increaseSound() {

	}
}
