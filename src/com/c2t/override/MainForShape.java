package com.c2t.override;

public class MainForShape {

	public static void main(String[] args) {
		Shape s;
		
		boolean isRectangle = true;
		
		if(isRectangle){
			s = new Rectangle();
		}else{
			s = new Circle();
		}
		
		s.draw();
		
	}

}
