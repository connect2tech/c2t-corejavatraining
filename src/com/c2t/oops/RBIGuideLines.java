package com.c2t.oops;

public interface RBIGuideLines {
	void debit();
	void credit();
}
