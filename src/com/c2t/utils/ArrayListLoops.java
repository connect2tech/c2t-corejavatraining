package com.c2t.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

public class ArrayListLoops {
	public static void main(String[] args) {
		// create list
		List<String> crunchifyList = new ArrayList<String>();
		//List crunchifyList = new ArrayList();
		
		Integer val = new Integer(100);

		// add 4 different values to list
		crunchifyList.add("eBay");
		crunchifyList.add("Paypal");
		crunchifyList.add("Google");
		crunchifyList.add("Yahoo");
		//crunchifyList.add(val);
		
				
		// iterate via "for loop"
		/*System.out.println("Size of list::"+crunchifyList.size());
		for(int i = 0; i<crunchifyList.size() ; i++){
			System.out.println(crunchifyList.get(i));
		}*/
		
		// iterate via "New way to loop"
		/*System.out.println("\n==> Advance For Loop Example..");
		for(String t : crunchifyList){
			System.out.println(t);
		}*/

		// iterate via "iterator loop"
		System.out.println("\n==> Iterator Example...");
		//Diff between for loop, iterator, and list iterator.
		Iterator<String> iter = crunchifyList.iterator();
		
		while(iter.hasNext()){
			String s = iter.next();
			System.out.println(s);
		}

		// iterate via "while loop"
		/*System.out.println("\n==> While Loop Example....");
		int i = 0;
		while (i < crunchifyList.size()) {
			System.out.println(crunchifyList.get(i));
			i++;
		}*/
		

		/*Enumeration <String> e = Collections.enumeration(crunchifyList);
		while(e.hasMoreElements()){
			String s = e.nextElement();
			System.out.println(s);
		}*/
		
	}
}
