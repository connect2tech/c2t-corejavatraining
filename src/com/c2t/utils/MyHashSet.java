package com.c2t.utils;

import java.util.*;

public class MyHashSet {
	public static void main(String[] args) {
		Set <String> s = new HashSet<String>();
		s.add("A");
		s.add("B");
		s.add("B");
		
		System.out.println(s);
		
		Iterator <String> iter = s.iterator();
		while(iter.hasNext()){
			String str = iter.next();
			System.out.println(str);
		}
	}
}
