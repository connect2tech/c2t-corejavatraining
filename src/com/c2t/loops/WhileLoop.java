package com.c2t.loops;

public class WhileLoop {
	public static void main(String[] args) {
		
		int a = 5;
		
		while (a>1){
			System.out.println("Testing the value of a ::"+a);
			a = a - 1;
		}
		
	}
}


//what ;; represent in the parameter