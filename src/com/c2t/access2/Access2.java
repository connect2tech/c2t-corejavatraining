package com.c2t.access2;

import com.c2t.access1.Access1;


class SubClass extends Access1{
	
	void display(){
		
		SubClass sub = new SubClass();
		System.out.println(sub.var);
	}
	
}

public class Access2 {
	
	public static void main(String[] args) {
		SubClass s = new SubClass();
		s.display();
		
	}
	
	
	
}
