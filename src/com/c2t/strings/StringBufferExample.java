package com.c2t.strings;

public class StringBufferExample {
	public static void main(String[] args) {
		//It is mutable or can be modified...
		StringBuffer sbuf = new StringBuffer("AB");
		
		sbuf.append("XYZ");
		
		System.out.println(sbuf);
	}
}
