package com.c2t.io;

import java.io.FileOutputStream;

public class WritingToFile {
	public static void main(String[] args) {
		try {
			
			FileOutputStream fos = new FileOutputStream("d://testing.txt");
			fos.write(97);//a
			fos.close();
			
			System.out.println("Completed...");
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
