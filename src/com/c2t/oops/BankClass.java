package com.c2t.oops;



class HDFCBank implements RBIGuideLines {
	public void debit() {
		System.out.println("debit");
	}

	public void credit() {
		System.out.println("credit");
	}
}

public class BankClass {
	public static void main(String[] args) {
		RBIGuideLines hdfc = new HDFCBank();
		hdfc.debit();
		hdfc.credit();
	}
}