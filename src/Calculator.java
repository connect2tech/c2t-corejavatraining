
public class Calculator {
	
	void display(int a){
		System.out.println(a);
	}
	
	void add(int a, int b) {
		System.out.println(a + b);
	}

	void add(int a, int b, int c) {
		System.out.println(a + b + c);
	}
	
	void add(String a, String b){
		System.out.println(a + b);
	}
	
	public static void main(String[] args) {
		Calculator c = new Calculator();
		c.add(10, 20);
		c.add(10, 20,30);
		c.add("Naresh ", "Java");
		
		c.display(10);
	}
}
