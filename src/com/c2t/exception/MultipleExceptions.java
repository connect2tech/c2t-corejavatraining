package com.c2t.exception;

import java.util.Scanner;

public class MultipleExceptions {
	public static void main(String[] args) {
		int b = 20;
		int c = 0;

		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();

		System.out.println("Performing division !!!");

		try {
			//c = b / a;
		} catch (ArithmeticException e) {
			System.out.println(e);
			System.out.println("Action1");
		} catch (NullPointerException e) {
			System.out.println(e);
			System.out.println("Action2");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e);
			System.out.println("Action3");
		}
		catch(Exception e){
			
		}

		System.out.println("c=" + c);

	}
}
