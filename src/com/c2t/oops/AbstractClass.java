package com.c2t.oops;


//Cannot instantiate the type AbstractConcept
abstract class AbstractConcept{
	
}

abstract class AbstractConcept1{
	void display(){
		System.out.println("display method!!!");
	}
	
	//This method requires a body instead of a semicolon
	abstract void hello(int a);
	
	//abstract void hai(int a);
}

//T he type SubAbstractConcept1 must implement the inherited abstract 
// method AbstractConcept1.hello(int)
class SubAbstractConcept1 extends AbstractConcept1{
	void hello(int a){
		System.out.println("Inside hello !!!");
	}
}

public class AbstractClass {
	public static void main(String[] args) {
		//AbstractConcept abs = new AbstractConcept();
		//AbstractConcept1 abs1 = new AbstractConcept1();
		//abs1.display();
		
		AbstractConcept1 c1 = new SubAbstractConcept1();
		c1.hello(10);
	}
}
