package com.c2t.datatypes;

public class Overflow {
	public static void main(String[] args) {
		// Overflow
		int a = 128;
		byte b = (byte) a;
		System.out.println(a);
		System.out.println(b);
	}
}
