package com.c2t.oops;

class Person1{
	String name;
	static String instructor;
	
	static{
		System.out.println("i am in static block");
	}
	
	Person1(){
		System.out.println("constructor...");
	}
	
	void method(){
		System.out.println("name="+name);
		System.out.println("instructor="+instructor);
	}
	
	static int display(){
		System.out.println("display");
		
		return 10;
	}
}

public class StaticConcepts {
	public static void main(String[] args) {
		Person1 p1 = new Person1();
		/*p1.name = "vara";
		p1.instructor = "naresh";
		//p1.method();
		
		Person1 p2 = new Person1();
		p2.name = "Ambujam";
		//p2.method();
		
		System.out.println(Person1.instructor);
		
		System.out.println(Person1.display());*/
	}
}
