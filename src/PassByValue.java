
public class PassByValue {
	public static void main(String[] args) {
		int a = 10;
		int b = 20;

		System.out.println("a="+a);
		System.out.println("b="+b);
		add(a,b);
		System.out.println("a="+a);
		System.out.println("b="+b);
				
	}
	
	static void add(int c, int d){
		
		c = 100;
		d = 200;
		
	    System.out.println(c+d);
	}
}
