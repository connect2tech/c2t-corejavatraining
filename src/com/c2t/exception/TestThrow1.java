package com.c2t.exception;

public class TestThrow1 {

	public static void main(String args[]) {
		try {
			checkAgeLimit();
		} catch (RuntimeException e) {
			System.out.println(e.toString());
		}
	}

	static void checkAgeLimit() {
		int age = 10;

		if (age < 18) {
			throw new RuntimeException("Below voting age limit !!!");
		} else {
			System.out.println("You are allowed to vote...");
		}
		System.out.println("Program completed....");
	}
}
