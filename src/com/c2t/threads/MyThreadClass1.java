package com.c2t.threads;

class CreatingThread extends Thread {
	public void run() {
		System.out.println("I am thread");
		System.out.println("My Name in run is ::" + Thread.currentThread().getName());
	}
}

public class MyThreadClass1 {
	public static void main(String[] args) {

		System.out.println("My Name in main is ::" + Thread.currentThread().getName());

		CreatingThread t = new CreatingThread();
		t.start();
		//t.run();
	}
}
