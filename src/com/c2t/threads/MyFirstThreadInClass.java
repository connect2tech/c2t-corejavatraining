package com.c2t.threads;

class MyClass{
	void display(){
		System.out.println("Inside the display..."+Thread.currentThread().getName());
	}
}

class MyThreadE3 extends Thread{
	public void run(){
		System.out.println("Inside the run..."+Thread.currentThread().getName());
	}
}

class MyThreadInterface implements Runnable{
	public void run(){
		System.out.println("Inside the run..."+Thread.currentThread().getName());
	}
}

//Indepdendent past of execution.
public class MyFirstThreadInClass {
	public static void main(String[] args) {

		System.out.println("Inside the main..."+Thread.currentThread().getName());
		
		MyThreadInterface i = new MyThreadInterface();
		Thread t = new Thread(i);
		t.start();
	
		/*MyClass m = new MyClass();
		m.display();*/
		
		//Causes this thread to begin execution; 
		//the Java Virtual Machine calls the run method of this thread.
		//MyThreadE3 t = new MyThreadE3();
		//t.start();
		//t.run();
		
		
	
	}
}
