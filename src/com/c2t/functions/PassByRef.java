package com.c2t.functions;

class Employee{
	String name;
	
	int a = 10;
	
	void setName(String e){
		name = e;
	}
	
	void modifyName(Employee x){
		x.name = "NC";
		//this
	}
	
	void add(int a, int b){
		
	}
}

public class PassByRef {
	public static void main(String[] args) {
		
		Employee aaa;
		
		Employee e = new Employee();
		e.setName("Naresh");
		
		System.out.println(e.name);
		
		e.add(10, 20);
		
		e.modifyName(e);
		
		System.out.println(e.name);
	}
}
