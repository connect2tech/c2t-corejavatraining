package com.c2t.wrapper;

public class MathWrapper {
	public static void main(String[] args) {
		int i = 80;
		int j = -25;

		/*
		 * To find absolute value of int, use static int abs(int i) method.
		 *
		 * It returns the same value if the agrument is non negative value,
		 * otherwise negation of the negative value.
		 *
		 */

		int absJ = Math.abs(j);
		System.out.println("absJ="+absJ);

		
		
		float f1 = 10.40f;
		float f2 = -50.28f;

		/*
		 * To find absolute value of float, use static float abs(float f)
		 * method.
		 *
		 * It returns the same value if the agrument is non negative value,
		 * otherwise negation of the negative value.
		 *
		 */
		System.out.println("Absolute value of " + f1 + " is :" + Math.abs(f1));
		System.out.println("Absolute value of " + f2 + " is :" + Math.abs(f2));

		double d1 = 43.324;
		double d2 = -349.324;
		/*
		 * To find absolute value of double, use static double abs(double d)
		 * method.
		 *
		 * It returns the same value if the agrument is non negative value,
		 * otherwise negation of the negative value.
		 *
		 */
		System.out.println("Absolute value of " + d1 + " is :" + Math.abs(d1));
		System.out.println("Absolute value of " + d2 + " is :" + Math.abs(d2));

		long l1 = 34;
		long l2 = -439;
		/*
		 * To find absolute value of long, use static long abs(long l) method.
		 *
		 * It returns the same value if the agrument is non negative value,
		 * otherwise negation of the negative value.
		 *
		 */
		System.out.println("Absolute value of " + l1 + " is :" + Math.abs(l1));
		System.out.println("Absolute value of " + l2 + " is :" + Math.abs(l2));

		/*
		 * To find minimum of two int values, use static int min(int a, int b)
		 * method of Math class.
		 */

		System.out.println(Math.min(34, 45));

		/*
		 * To find minimum of two float values, use static float min(float f1,
		 * float f2) method of Math class.
		 */
		System.out.println(Math.min(43.34f, 23.34f));

		/*
		 * To find minimum of two double values, use static double min(double
		 * d2, double d2) method of Math class.
		 */
		System.out.println(Math.min(4324.334, 3987.342));

		/*
		 * To find minimum of two long values, use static long min(long l1, long
		 * l2) method of Math class.
		 */

		System.out.println(Math.max(48092840, 4230843));
	}
}
