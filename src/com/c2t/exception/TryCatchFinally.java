package com.c2t.exception;

public class TryCatchFinally {
	public static void main(String[] args) {
		try {

		} finally {

		}
		
		//A try block should have at least one catch or a finally.
	}
}
