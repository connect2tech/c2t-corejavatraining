class ThisExample{
	String name;
	
	void setName(String name){
		this.name = name;
	}
	
	void displayName(){
		System.out.println("name="+name);
	}
}



public class UsingThis {
	public static void main(String[] args) {
		ThisExample e = new ThisExample();
		e.setName("This Pointer");
		
		e.displayName();
	}
}
