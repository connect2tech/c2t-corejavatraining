final class Final {
	final String name = "Naresh";
	String company = "XYZ";

	void showDetails() {
		// name = "NC";//The final field Final.name cannot be assigned
		company = "CC";
		System.out.println("showDetails!!!");
	}

	final void method() {

	}
}

//The type SubFinal cannot subclass the final class Final
class SubFinal{
	void showDetails() {
		// some code
	}
	//	- Cannot override the final method 
	/*void method() {

	}*/
}

public class UnderstandinFinal {
	public static void main(String[] args) {
		Final f = new Final();
		f.showDetails();
	}
}
