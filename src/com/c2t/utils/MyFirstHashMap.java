package com.c2t.utils;

import java.util.*;

class Employee{
	
	String name;
	String company;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	
	
}

public class MyFirstHashMap {
	public static void main(String[] args) {
		
		
		Map <String, String> m = new HashMap<String, String>();
		
		Employee e1 = new Employee();
		e1.setName("Shilpa");
		e1.setCompany("");
		
		m.put("id1", "Puneet, Cogni, Selenium");
		m.put("id2", ", Capg, Selenium");
		m.put("id2", "Mansa, Capg, Selenium");
		System.out.println(m);
		
		Set <String> keys = m.keySet();
		Iterator <String> iter = keys.iterator();
		
		while(iter.hasNext()){
			String key = iter.next();
			String val = m.get(key);
			System.out.println(val);
		}
		
		/*for (String key : loans.keySet()) {
			System.out.println("------------------------------------------------");
			System.out.println("Iterating or looping map using java5 foreach loop");
			System.out.println("key: " + key + " value: " + loans.get(key));
		}*/

		
		/*Set<String> keySet = loans.keySet();

		Iterator<String> keySetIterator = keySet.iterator();

		while (keySetIterator.hasNext()) {
			System.out.println("------------------------------------------------");
			System.out.println("Iterating Map in Java using KeySet Iterator");
			String key = keySetIterator.next();
			System.out.println("key: " + key + " value: " + loans.get(key));
		}*/

		/*
		 * Set<Map.Entry<String, String>> entrySet = loans.entrySet();
		 * 
		 * for (Map.Entry entry : entrySet) {
		 * System.out.println("------------------------------------------------"
		 * ); System.out.
		 * println("looping HashMap in Java using EntrySet and java5 for loop");
		 * System.out.println("key: " + entry.getKey() + " value: " +
		 * entry.getValue()); }
		 */

		/*Set<Map.Entry<String, String>> entrySet1 = loans.entrySet();
		
		Iterator<Map.Entry<String, String>> entrySetIterator = entrySet1.iterator();
		
		while (entrySetIterator.hasNext()) {
			System.out.println("------------------------------------------------");
			System.out.println("Iterating HashMap in Java using EntrySet and Java iterator");
			Map.Entry entry = entrySetIterator.next();
			System.out.println("key: " + entry.getKey() + " value: " + entry.getValue());
		}*/

	}
}
