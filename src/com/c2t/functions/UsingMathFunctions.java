package com.c2t.functions;

import java.lang.Math;

public class UsingMathFunctions {
	public static void main(String[] args) {
		
		double a = 10.5;
		double b = 20.5;
		double c = -100.5;
		
		double minValue = Math.min(a, b);
		System.out.println(minValue);
		
		double absValue = Math.abs(c);
		System.out.println(absValue);
	}
}
