class Concepts {

	final String name = "";

	void setName() {
		// name = "New value of name..";
	}

}

class Sub extends Concepts {
	void setName() {
		// name = "New value of name..";
	}
}

public class FinalConcepts {

	public static void main(String[] args) {
		Concepts c = new Concepts();
		c.setName();

		System.out.println(c.name);

	}
}
