package com.c2t.oops;

class MyBike{
	void startEngine(){
		System.out.println("startEngine");
	}
}

class Bajaj extends MyBike{
	
}

class BMW extends MyBike{
	void startEngine(){
		System.out.println("startEngine of BMW");
	}
}

public class MainOverride {

	public static void main(String[] args) {
		
		MyBike bmw2 = new BMW();
		bmw2.startEngine();
	}
}
