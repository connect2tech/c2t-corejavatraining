package com.c2t.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ReadingFromFile {
	public static void main(String[] args) {
		//FileInputStream(File file)
		//File(String pathname)
		
		File f = new File("d://newfile.txt");
		try {
			FileInputStream fis = new FileInputStream(f);
			
			int c = fis.read();
			System.out.println((char)c);
			
			fis.close();
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
