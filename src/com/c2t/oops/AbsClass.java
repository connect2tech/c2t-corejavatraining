package com.c2t.oops;

class Concrete{
	void display(){
		
	}
}

abstract class MyClassAbs {
	abstract void paint();
	
	void hello(){
		
	}
}

class SubAbstract extends MyClassAbs{
	void paint(){
		System.out.println("Hello...");
	}
}


public class AbsClass {
	public static void main(String[] args) {
		//concrete class.
		Concrete c = new Concrete();
		
		MyClassAbs a = new SubAbstract();
		a.paint();
		
	}
}
