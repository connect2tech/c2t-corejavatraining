package com.c2t.exception;

public class MyFirstExceptionProgram {
	public static void main(String[] args) {

		int a[] = { 1, 2, 3, 4, 5 };

		try {
			System.out.println(a[6]);
		} 
		
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("ArrayIndexOutOfBoundsException");
		} 
		catch (Exception e) {
			System.out.println("Exception");
		}
		

		System.out.println("Execution successful....");

	}
}
