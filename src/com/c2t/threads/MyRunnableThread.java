package com.c2t.threads;

class RunnableThread implements Runnable {

	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(1000);
				System.out.println("i am inside run...");
				String salsa = Thread.currentThread().getName();
				System.out.println("run==" + salsa);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
}

public class MyRunnableThread {
	public static void main(String[] args) {
		RunnableThread r = new RunnableThread();
		Thread t = new Thread(r);
		t.start();
	}
}
