//concrete.
public class ParameterizedConstructor {
	ParameterizedConstructor(int a, int b) {
		System.out.println(a + b);
	}

	ParameterizedConstructor() {

	}
	
	public static void main(String[] args) {
		ParameterizedConstructor p = new ParameterizedConstructor();
	}
}
