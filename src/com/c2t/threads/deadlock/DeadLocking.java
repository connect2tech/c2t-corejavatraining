package com.c2t.threads.deadlock;

class Thread1 extends Thread {

	String apple;
	String banana;

	Thread1(String s1, String s2) {
		apple = s1;
		banana = s2;
	}
	
	int i;

	public void run() {
		while (true) {
			/*synchronized (apple) {
				synchronized (banana) {*/
					System.out.println("Inside the run..."+Thread.currentThread().getName());
					System.out.println("i = "+ ++i);
					System.out.println("apple, banana");
			/*	}
			}*/
		}
	}
}

class Thread2 extends Thread {
	String apple;
	String banana;

	Thread2(String s1, String s2) {
		apple = s1;
		banana = s2;
	}
	
	int j;

	public void run() {
		while (true) {
			/*synchronized (banana) {
				synchronized (apple) {*/
					System.out.println("Inside the run..."+Thread.currentThread().getName());
					System.out.println(" j =" +  ++j);
					System.out.println("banana, apple");
			/*	}
			}*/
		}
	}
}

public class DeadLocking {

	public static void main(String[] args) {

		String apple = "apple";
		String banana = "banana";

		Thread1 t1 = new Thread1(apple, banana);
		Thread2 t2 = new Thread2(apple, banana);

		t1.start();
		t2.start();
	}

}
