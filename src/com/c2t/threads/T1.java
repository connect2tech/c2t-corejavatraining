package com.c2t.threads;

class T2 extends Thread {
	void m1() {
		System.out.println("T2  ---" + Thread.currentThread().getName());
	}

	public void run() {
		System.out.println("run  ---" + Thread.currentThread().getName());
	}
}

public class T1 {
	public static void main(String[] args) {

		System.out.println(Thread.currentThread().getName());
		T2 t = new T2();
		/*
		 * t.m1(); t.run();
		 */

		t.start();
	}

}
