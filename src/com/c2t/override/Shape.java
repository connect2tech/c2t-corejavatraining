package com.c2t.override;

public interface Shape {
	void draw();
}

class Circle implements Shape {
	public void draw() {
		System.out.println("Circle");
	}
}

class Rectangle implements Shape {
	public void draw() {
		System.out.println("Rectangle");
	}
}
