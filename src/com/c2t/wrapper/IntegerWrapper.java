package com.c2t.wrapper;

public class IntegerWrapper {

	public static void main(String[] args) {

		//Integer to String
		
		int a = 100;
		
		Integer i = new Integer(a);
		int val2 = i.intValue();
		float f = i.floatValue();
		String s = i.toString();
		
		int value = Integer.valueOf("100");
		
		
		//Integer to primitive
		
		String valFromExcel = "1000.5";
		
		Integer intObj = new Integer(valFromExcel);
	    //use byteValue method of Integer class to convert it into byte type.
		int val = intObj.intValue();
	   /* byte b = intObj.byteValue();
	    System.out.println(b);
	    
	    int valFromExcel2 = intObj.intValue();
	    System.out.println(valFromExcel2);
	    
	    int a = Integer.parseInt(valFromExcel);*/
	   
		
		/*Integer intObj = new Integer(100);
	    
	    //use shortValue method of Integer class to convert it into short type.
	    short s = intObj.shortValue();
	    System.out.println(s);
	   
	    //use intValue method of Integer class to convert it into int type.
	    int i = intObj.intValue();
	    System.out.println(i);
	   
	    //use floatValue method of Integer class to convert it into float type.
	    float f = intObj.floatValue();
	    System.out.println(f);
	 
	    //use doubleValue method of Integer class to convert it into double type.
	    double d = intObj.doubleValue();
	    System.out.println(d);
	    
		
		/*
	    //We can convert String to Integer using following ways.
	    //1. Construct Integer using constructor.
	    Integer intObj1 = new Integer("100");
	    System.out.println(intObj1);
	   
	    //String to Integer
	    //2. Use valueOf method of Integer class. This method is static.
	    String str = "100";
	    Integer intObj2 = Integer.valueOf(str);
	    System.out.println(intObj2);
	   
	    //Please note that both method can throw a NumberFormatException if
	    //string can not be parsed.
	     */

	}

}
