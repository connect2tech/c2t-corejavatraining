package com.c2t.utils;

import java.util.Hashtable;

public class HashTableWithNull {
	
	static String s1 = null;
	static String s2 = null;
	
	public static void main(String[] args) {
		Hashtable<String, String> ht = new Hashtable<String, String>();
		ht.put(null, "This is null");
		ht.put("key", s1);
		ht.put(s2, "abc");
		
		System.out.println(ht);
	}
}
