package com.c2t.arrays;

public class MultidimensionalArray {
	public static void main(String[] args) {

		int[][] matrix1 = new int[2][2];
		
		matrix1[0][0] = 100;
		matrix1[0][1] = 200;
		matrix1[1][0] = 300;
		matrix1[1][1] = 400;
		
		int a = 10;
		int b = 20;
		int c = 0;
		
		boolean bool = true;
		
		if(bool){
			c = a;
		}else{
			c=b;
		}
		

		int[][] matrix2 = { 
				{ 10, 20 },// Row0
				{ 30, 40 } // Row1
		};
		
		/*matrix1[0][0] = 100;

		int matrix3[][];

		matrix1[0][1] = 100;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.println(matrix1[i][j]);
			}
		}*/
		
		
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				System.out.println("value of i is="+i+ " , value of j is = "+j);
				System.out.println(matrix2[i][j]);
			}
		}

	}
}
