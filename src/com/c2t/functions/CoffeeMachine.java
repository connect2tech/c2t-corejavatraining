package com.c2t.functions;

public class CoffeeMachine {
	
	void initalizeMachine(){
		System.out.println("Get water, if not available !!!");
		System.out.println("Get beans, if not available !!!");
		System.out.println("Get milk, if not available !!!");
	}
	
	void startMachine(){
		int a = 10;//stord in the stack.
		String s = new String("Coffee!!!");
		initalizeMachine();
		
		System.out.println("Warm up water !!!");
		System.out.println("Grind coffee beans !!!");
		System.out.println("Warm up milk !!!");
	}
	
	int countBeans(){
		return 10;
	}
	
	String dispenseCoffee(){
		String d = "Mixing water and beans and milk";
		String b = "";
		return b;
	}
	
	void callByValue(int basePriceOfCoffee){
		basePriceOfCoffee = basePriceOfCoffee + 5;
		System.out.println("basePriceOfCoffee="+basePriceOfCoffee);
	}
	
	public static void main(String[] args) {
		int price = 10;
		System.out.println("price1="+price);
		CoffeeMachine cm = new CoffeeMachine();
		cm.callByValue(price);
		System.out.println("price2="+price);
		/*cm.startMachine();
		String c = cm.dispenseCoffee();
		System.out.println(c);
		int count = cm.countBeans();*/
	}
}
