package com.c2t.generic;

class GenericClass<T> {
	T t;

	public GenericClass(T t) {
		this.t = t;
	}

	public void setT(T t) {
		this.t = t;
	}

	public T getT() {
		return t;
	}
}