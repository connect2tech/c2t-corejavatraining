
public class MethodOverloading {
	public static void main(String[] args) {
		
		MethodOverloading m = new MethodOverloading();
		m.add(10, 20);
		
		m.add(10.5f, 10.6f, 10.7f, 10.8f);
		
	}
	
	void add(int a, int b){
		System.out.println(a+b);
	}
	
	void add (int a , String b){
		
	}
	
	void add(String a , int b){
		
	}
	
	void add(int a, int b, int c){
		System.out.println(a+b+c);
	}
	
	void add(float a, float b, float c, float d){
		System.out.println(a+b+c+d);
	}
	
}
