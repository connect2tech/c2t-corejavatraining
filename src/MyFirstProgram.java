class Employee1000 {

	String name;

	void display() {
		System.out.println("Display Method!!!");
		System.out.println("Hello ::" + name);
	}

}

class Calci {
	
	void changeName(Employee1000 emp ) {
		emp.name = "Naresh";
	}
}

public class MyFirstProgram {
	public static void main(String[] args) {

		Employee1000 e1 = new Employee1000();
		e1.name = "Gangaraju";
		e1.display();
		
		Calci c = new Calci();
		c.changeName(e1);
		
		e1.display();

	}
}
