package com.c2t.oops;

abstract public class Shape {
	abstract void draw();

	void display() {
	}
}

class NewCircle extends Shape{
	void draw(){
		System.out.println("draw circle....");
	}
	
	void display() {
	}
}

class NewTriangle extends Shape{
	void draw(){
		System.out.println("draw NewTriangle....");
	}
}