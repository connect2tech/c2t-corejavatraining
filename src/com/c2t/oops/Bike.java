package com.c2t.oops;

public class Bike {
	
	static {
		System.out.println("static block");
	}

	String model;  //null
	String regNumber; //null
	float capacity; //0.0
	
	static String company;
	
	public Bike(){
		model = "m1";
		regNumber = "abc";
		capacity = 10.5f;
	}
	
	Bike(String m, String reg, Float c){
		model = m;
		regNumber = reg;
		capacity = c;
		
		System.out.println("I am parameterized contructor!!!");
	}
	
	Bike(String m, String reg){
		model = m;
		regNumber = reg;
		
		System.out.println("I am parameterized contructor 2!!!");
	}


	void start() {
		System.out.println("start");
	}

	void speedUp() {
		System.out.println("Speed Up");
	}

	void display() {
		System.out.println("model: " + model);
		System.out.println("regNumber: " + regNumber);
		System.out.println("capacity: " + capacity);
		System.out.println("company="+company);
	}
	
	static void showValue(){
		System.out.println("company="+company);
		//System.out.println("model="+model);
	}
}
