package com.c2t.oops;

class Mercedes extends Car {
	void startEngine() {
		System.out.println("Start Engine of Merc...");
	}
}

public class MercedesMain {
	public static void main(String[] args) {/*

		boolean b = true;
		Car car;

		Mercedes m = new Mercedes();
		m.startEngine();//Compiled...

		Car c = new Car();
		c.startEngine();

		if (b) {
			car = new Mercedes();
		} else {
			car = new Car();
		}

		car.startEngine();
	*/}
}
