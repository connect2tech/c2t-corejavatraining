package com.c2t.interfaces;

public interface MyRemote {
	
	String name = "Remote";
	
	void increaseVolume();
	void decreaseVolume();
	
	void on();
	void off();
}
