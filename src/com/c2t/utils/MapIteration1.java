package com.c2t.utils;

import java.util.*;

public class MapIteration1 {
	public static void main(String[] args) {
		HashMap<String, String> loans = new HashMap<String, String>();
		loans.put("home loan", "citibank");
		loans.put("personal loan", "Wells Fargo");

		Set<String> keySet = loans.keySet();

		for (String key : keySet) {
			System.out.println("key::" + key + " , value::" + loans.get(key));
		}

		System.out.println("==============================================");

		Iterator<String> iter = keySet.iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			System.out.println("key::" + key + " , value::" + loans.get(key));
		}
	}
}
