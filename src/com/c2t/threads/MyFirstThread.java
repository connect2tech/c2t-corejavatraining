package com.c2t.threads;

class MyThread extends Thread {
	public void run() {

		for (int i = 0; i < 5; i++) {
			System.out.println("I am a thread");
			System.out.println("My Name inside run is :" + Thread.currentThread().getName());

			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
}

class MyThreadRunnable implements Runnable {
	public void run() {
		System.out.println("I am runnable thread. My Name is ::" + Thread.currentThread().getName());
	}
}

public class MyFirstThread {
	public static void main(String[] args) {
		/*MyThread t = new MyThread();
		t.start();
		for (int i = 0; i < 5; i++) {

			System.out.println("My Name(MyTread) is :" + Thread.currentThread().getName());
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}*/
		
		
		MyThreadRunnable run = new MyThreadRunnable();
		Thread t = new Thread(run, "My Name is Runnable");
		t.start();
		System.out.println("My Name is ::"+ Thread.currentThread().getName());

	}
}
