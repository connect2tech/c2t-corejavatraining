package com.c2t.abs;

abstract class Shape{
	
	//The abstract method draw in type Shape can only be defined by an abstract class
	abstract void draw();// Declaration of method.
	
	//Declaration and definition of method.
	void display(){
		System.out.println("display");
	}
}

//The type Circle must implement the inherited abstract method Shape.draw()
class Circle extends  Shape{
	void draw(){
		System.out.println("Circle");
	}
}


public class MainForAbstract {
	public static void main(String[] args) {
		
		Shape s = new Circle();
		s.draw();
		
	}
}
