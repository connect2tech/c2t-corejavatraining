package com.c2t.loops;

public class IfElse {
	
	public static void main(String[] args) {
		int balance = 1500;
		
		String message;
		
		if (balance > 1000){
			System.out.println("Withdraw money from atm");
			message = "Sufficient balance";
		}else{
			System.out.println("Insufficient balance...");
			message = "In-Sufficient balance";
		}
		
		System.out.println(message);
	
	}

}
