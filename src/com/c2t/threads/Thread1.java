package com.c2t.threads;

class Employee extends Thread {
	
	public void run() {

		for (int i = 0; i < 10; i++) {
			try {
				sleep(1000);
				System.out.println("i am inside run...");
				String salsa = Thread.currentThread().getName();
				System.out.println("run==" + salsa);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

}

// It is an independent path of execution...
public class Thread1 {
	public static void main(String[] args) {
		Employee e = new Employee();
		e.setName("HelloThread");
		// e.run();
		e.start();

		for (int j = 0; j < 10; j++) {
			try {
				String name = Thread.currentThread().getName();
				System.out.println("name==" + name);
				Thread.sleep(1000);
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
	}
}
