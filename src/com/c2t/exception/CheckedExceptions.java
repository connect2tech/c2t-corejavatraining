package com.c2t.exception;

import java.io.*;

public class CheckedExceptions {
	public static void main(String[] args) {

		// Multiple markers at this line
		// - Resource leak: 'fis' is never closed
		// - Unhandled exception type

		// readFromFile();
		
		
		File f = new File("");
		
		try {
			FileInputStream fis = new FileInputStream(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	/*static void readFromFile() throws FileNotFoundException {
		File f = new File("");
		FileInputStream fis = new FileInputStream(f);
	}*/
}
