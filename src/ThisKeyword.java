
class UnderstandingThis {
	String name;

	UnderstandingThis(String name) {
		int a = 10;
		name = name;
		a = a;
		
		this.name = name;
	}

	void display() {
		System.out.println("i am display");
		System.out.println("this = " + this);
	}

}

public class ThisKeyword {
	public static void main(String[] args) {
		UnderstandingThis t = new UnderstandingThis("Apeksha");
		t.display();

		System.out.println(t);
	}
}
