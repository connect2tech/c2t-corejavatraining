package com.c2t.oops;


class Person{
	
	String name;
	int age;
	char gender;
	
	Person(String n, int a, char g){
		System.out.println("I am in 2nd constructor!!!");
		name = n;
		age = a;
		gender = g;
	}
	
	Person(){
		System.out.println("I am person!!!");
		
		name = "Person";
		age = 25;
		gender = 'm';
	}
	
	void walk(){
		System.out.println(name + " can walk !!!");
	}
	
	void details(){
		System.out.println("name ::"+name);
		System.out.println("age ::"+age);
		System.out.println("gender ::"+gender);
	}
	
}

public class MyClass {
	public static void main(String[] args) {
		
		/*Person bobby = new Person();
		
		ambujam.details();
		radha.details();*/
		
		/*Person ambujam = new Person();
		ambujam.details();
		
		Person vara = new Person();
		vara.details();
		
		
		vara.name = "Vara";
		vara.age = 20;
		vara.gender = 'm';
		
		vara.details();
		vara.walk();*/
		
		
		/*Person senthil = new Person();
		
		senthil.name = "senthil";
		senthil.age = 22;
		senthil.gender = 'm';
		
		senthil.details();
		senthil.walk();*/
		
		
	}
}
